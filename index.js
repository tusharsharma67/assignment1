// import dependencies
const express = require('express');
const path = require('path');

// set up expess validator
const { check, validationResult } = require('express-validator');

// set up mongodb connection
const mongoose = require('mongoose');
const { stringify } = require('querystring');
mongoose.connect('mongodb://localhost:27017/foodmart', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// set up the model for the order
const Order = mongoose.model('Order', {
    name: String,
    email: String,
    phone: String,
    deliveryAddress: String,
    lays: String,
    milk: String,
    bread: String,
    shippingCharges: String,
    subTotal: Number,
    tax: Number,
    total: Number
});

// set up variables
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));

// set path to public folders, view folders and ejs view engine
myApp.set('views', path.join(__dirname, 'views'));
myApp.use(express.static(__dirname + '/public'));
myApp.set('view engine', 'ejs');

// set up different routes (pages) of the website
// render the home page
myApp.get('/', function (req, res) {
    res.render('home');
});

var phoneRegex = /^[0-9]{3}\-?[0-9]{3}\-?[0-9]{4}$/;
var postCodeRegex = /^[A-Za-z]{1}[0-9]{1}[A-Z]{1}\s[0-9]{1}[A-Z]{1}[0-9]{1}$/

myApp.post('/', [
    check('name', 'Must have a name').notEmpty(),
    check('email', 'Must enter email in test@domain.com format').isEmail(),
    check('phone', 'Phone Number should be in format of xxx-xxx-xxxx or atleast 10 digits').matches(phoneRegex),
    check('street', 'Must have an street name').notEmpty(),
    check('city', 'Must enter city name').notEmpty(),
    check('postCode', 'Must Enter Postcode in X0X 0X0 format only').matches(postCodeRegex),
    check('province', 'Must Select Province').notEmpty(),
    check('delivery', 'Must Select Delivery Time').notEmpty()

], function (req, res) {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors); // check what is the structure of errors
        res.render('home', {
            errors: errors.array()
        });
    }
    else {
        var name = req.body.name;
        var email = req.body.email;
        var phone = req.body.phone;
        var street = req.body.street;
        var city = req.body.city;
        var postCode = req.body.postCode;
        var province = req.body.province;
        var lays = req.body.lays;
        var milk = req.body.milk;
        var bread = req.body.bread;
        var delivery = req.body.delivery;

        lays = parseInt(lays);
        milk = parseInt(milk);
        bread = parseInt(bread);

        var subTotal = 0;
        var itemsPurchased = '';

        itemsPurchased += `<tr style="font-weight: bold;"><td>Item</td><td>Quantity</td><td>Unit Price</td><td>Total Price</td></tr>`;

        if (lays > 0) {
            subTotal += lays * 3;
            itemsPurchased += `<tr><td>Lay's</td><td>${lays}</td><td>$3</td><td>$${lays * 3}</td></tr>`;
        }
        else {
            lays = 0;
        }

        if (milk > 0) {
            subTotal += milk * 4;
            itemsPurchased += `<tr><td>Choclate Milk</td><td>${milk}</td><td>$4</td><td>$${milk * 4}</td></tr>`;
        }
        else {
            milk = 0;
        }

        if (bread > 0) {
            subTotal += bread * 3;
            itemsPurchased += `<tr><td>Bread</td><td>${bread}</td><td>$3</td><td>$${bread * 3}</td></tr>`;
        }
        else {
            bread = 0;
        }

        var shippingCharges = 0;
        switch (delivery) {
            case "1 Days":
                shippingCharges = 40;
                break;
            case "2 Days":
                shippingCharges = 30;
                break;
            case "3 Days":
                shippingCharges = 20;
                break;
            case "4 Days":
                shippingCharges = 10;
                break;
            case "5 Days":
                shippingCharges = 0;
                break;
        }

        subTotal += shippingCharges;
        itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Shipping Charges (${delivery})</td><td>$${shippingCharges}</td></tr>`;

        if (subTotal < 10) {
            var minimumPurchase = 'Minimum Purchase should be $10';
            res.render('home', { minimumPurchase });
        }
        else {
            var taxPercentage = 0;
            switch (province) {
                case "Alberta":
                    taxPercentage = 0.5;
                    break;
                case "British Columbia":
                    taxPercentage = 0.12;
                    break;
                case "Manitoba":
                    taxPercentage = 0.12;
                    break;
                case "New Brunswick":
                    taxPercentage = 0.15;
                    break;
                case "Newfoundland and Labrador":
                    taxPercentage = 0.15;
                    break;
                case "Nova Scotia":
                    taxPercentage = 0.15;
                    break;
                case "Ontario":
                    taxPercentage = 0.13;
                    break;
                case "Prince Edward Island":
                    taxPercentage = 0.15;
                    break;
                case "Quebec":
                    taxPercentage = 0.14975;
                    break;
                case "Saskatchewan":
                    taxPercentage = 0.11;
                    break;
            }
            var tax = subTotal * taxPercentage;
            var total = subTotal + tax;

            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Sub Total</td><td>$${subTotal}</td></tr>`;
            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Tax ${taxPercentage * 100}%</td><td>$${tax.toFixed(2)}</td></tr>`;
            itemsPurchased += `<tr style="font-weight: bold;"><td colspan="3">Total</td><td>$${total.toFixed(2)}</td></tr>`;

            var pageData = {
                name: name,
                email: email,
                phone: phone,
                deliveryAddress: street + " " + city + " " + province + " " + postCode,
                shippingCharges: shippingCharges,
                subTotal: subTotal,
                tax: tax,
                total: total,
                itemsPurchased: itemsPurchased
            }
            // create an object for the model Order
            var myOrder = new Order({
                name: name,
                email: email,
                phone: phone,
                deliveryAddress: street + " " + city + " " + province + " " + postCode,
                lays: `Quantity = ${lays} Price = $${lays * 3}`,
                milk: `Quantity = ${milk} Price = $${milk * 4}`,
                bread: `Quantity = ${bread} Price = $${bread * 3}`,
                shippingCharges: `${delivery} $${shippingCharges}`,
                subTotal: subTotal,
                tax: tax,
                total: total
            });
            // save the order
            myOrder.save().then(function () {
                console.log('New order created');
            });
            // display receipt
            res.render('home', pageData);
        }

    }
});

//orders page
myApp.get('/orders', function (req, res) {
    Order.find({}).exec(function (err, orders) {
        res.render('orders', { orders: orders });
    });

});

//author page
myApp.get('/author', function (req, res) {
    res.render('author', {
        name: 'Tushar Sharma',
        studentNumber: '8756307'
    });
});

// start the server and listen at a port
myApp.listen(8080);

//tell everything was ok
console.log('Everything executed fine.. website at port 8080....');